const API_MAPPING = {
  localhost: "http://localhost:8080",
  "sky-mavis-interview-front-end.herokuapp.com": "https://sky-mavis-interview-back-end.herokuapp.com"
};

export const ENV = process.env.NODE_ENV;
export const HOST_PORT = API_MAPPING[window.location.hostname];
