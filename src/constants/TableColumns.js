import { Tag } from 'antd';
import moment from "moment-timezone";

export const getTableColumns = (timezone) => {
    const columns = [
        {
          title: 'IP Address',
          dataIndex: 'ipAddress',
          key: 'ipAddress',
          render: text => {
            return (
              <Tag color='blue'>{text}</Tag>
            )
          }
        },
        {
          title: 'Browser Info',
          dataIndex: 'browserInfo',
          key: 'browserInfo',
        },
        {
          title: 'Last Visit Time',
          dataIndex: 'lastVisitTime',
          key: 'lastVisitTime',
          render: lastVisitTime => <p>{moment(lastVisitTime, "x").tz(timezone).format()}</p>
        },
        {
          title: 'Visit Amount',
          dataIndex: 'visitAmount',
          key: 'visitAmount',
        },
        {
          title: 'Operation System',
          dataIndex: 'operation',
          key: 'operation',
          render: (operation) => {
            return (
              <Tag color='green'>{operation}</Tag>
            )
          }
        },
        {
          title: 'Refferer',
          dataIndex: 'referrer',
          key: 'referrer',
        },
        {
          title: 'User Agent',
          dataIndex: 'userAgent',
          key: 'userAgent',
        },
    ];

    return columns;
} 