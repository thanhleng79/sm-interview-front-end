import axios from "axios";
import { HOST_PORT } from "../constants/Enviroment";

const CONNECTION_TIMEOUT = 300000;

export function httpGet(url, body) {
    return callApi(url, "GET", body);
}
  
export function httpPost(url, body) {
    return callApi(url, "POST", body);
}  

function callApi(url, method, body = null, responseType = null, mediatype) {
    
    const headers = {
      "Content-Type": mediatype || "application/json; charset=utf-8",
    };
  
    const meta = { method, headers, responseType, timeout: CONNECTION_TIMEOUT };
    if (body) {
      meta.data = JSON.stringify(body);
    }
  
    return new Promise((resolve, reject) => {
      axios(`${HOST_PORT}/${url}`, meta)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          if (
            error &&
            error.response &&
            error.response.status >= 400 &&
            error.response.status <= 500
          ) {
            resolve(error.response);
          } else {
            console.log(error);
            reject(error);
          }
        });
    });
  }