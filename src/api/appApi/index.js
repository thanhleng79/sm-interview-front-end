import { httpGet } from "..";

const BASE_URL = "api/v1"

export function getAppName() {
    return httpGet(BASE_URL + "/health");
} 

export function getClientInfo() {
    return httpGet(BASE_URL + "/client-info");
}

export function getClientInfoByIP(ipAddress) {
    return httpGet(BASE_URL + "/client-info-by-ip?ipAddress=" + ipAddress);
}

export function getLastHundredVisitors() {
    return httpGet(BASE_URL + "/last-hundred-visitor");
}

export function getMostHundredVisitors() {
    return httpGet(BASE_URL + "/most-hundred-visitor");
}