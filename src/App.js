import { useEffect, useState } from 'react';
import { getAppName, getClientInfo, getClientInfoByIP, getLastHundredVisitors, getMostHundredVisitors } from './api/appApi';
import moment from "moment-timezone";
import { Divider, Layout, Descriptions, Table, Tag, Skeleton, 
  Avatar, Form, Input, Button, Row, Col, notification } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import './App.css';
import { getTableColumns } from './constants/TableColumns';
const { Header, Content, Footer } = Layout;
const timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

const openNotificationWithIcon = type => {
  notification[type]({
    message: 'Not found',
    description: 'Can not found visitor with IP Address',
    duration: 5,
  });
};


function App() {
  const [form] = Form.useForm();
  const [appName, setAppName] = useState("");
  const [loadingClientInfo, setLoadingClientInfo] = useState(false);
  const [clientInfo, setClientInfo] = useState(null);
  const [loadingFind, setLoadingFind] = useState(false);
  const [findResult, setFindResult] = useState(null);
  const [loadingLast, setLoadingLast] = useState(false);
  const [lastHundredVisitors, setLastHundredVisitors] = useState([]);
  const [loadingMost, setLoadingMost] = useState(false);
  const [mostHundredVisitors, setMostHundredVisitors] = useState([]);

  const getClientInfoApp = () => {
    console.log("timezone: ", timezone);
    console.log("current moment: ", moment().tz(timezone).format())
    setLoadingClientInfo(true);
    getClientInfo().then(res => {
      console.log(res);
      if (res !=  null && res.data != null)
        setClientInfo(res.data);
    }).catch(e => console.log(e))
    .finally(() => setLoadingClientInfo(false))
  }

  const getLastHundredVisitorsApp = () => {
    setLoadingLast(true);
    getLastHundredVisitors().then(res => {
      if (res !=  null && res.data != null) {
        const results = res.data.map(item => {
          return {...item, key: item.id}
        });
        console.log("getLastHundredVisitorsApp: ", results);
        setLastHundredVisitors(results);
      }
    }).catch(e => console.log(e))
    .finally(() => setLoadingLast(false))
  }

  const getMostHundredVisitorsApp = () => {
    setLoadingMost(true);
    getMostHundredVisitors().then(res => {
      if (res !=  null && res.data != null) {
        const results = res.data.map(item => {
          return {...item, key: item.id}
        });
        console.log("getMostHundredVisitorsApp: ", results);
        setMostHundredVisitors(results);
      }
    }).catch(e => console.log(e))
    .finally(() => setLoadingMost(false))
  }

  useEffect(() => {
    getAppName().then(res => {
      console.log(res);
      if (res !=  null && res.data != null)
        setAppName(res.data);
    });
    getClientInfoApp();
    getLastHundredVisitorsApp();
    getMostHundredVisitorsApp();
  }, []);

  const handleSearchClientByIp = (values) => {
    setLoadingFind(true);
    getClientInfoByIP(values.ipAddress).then(res => {
      console.log("handleSearchClientByIp res: ", res);
      if (res !=  null && res.data != null && res.data != '') {
        setFindResult(res.data);
      }
      else {
        openNotificationWithIcon('warning');
      }
    }).catch(e => console.log(e))
    .finally(() => setLoadingFind(false))
  }

  const handleClearForm = () => {
    form.resetFields();
    setFindResult(null);
  }

  return (
    <Layout className="layout">
      <Header style={{
            position: 'fixed',
            zIndex: 1,
            width: '100%',
            backgroundColor:'#fff',
            color:'#262626',
            fontWeight:'bold',
            borderBottom: '1px solid #f0f0f0',
            padding: '0 100px'
          }}>
            <div style={{display:'flex'}}>
              <Avatar style={{margin:'15px 20px 0 0'}} src="https://joeschmoe.io/api/v1/random" />
              <h3>{appName}</h3>
            </div>
      </Header>
      <Content style={{ padding: '0 100px' }}>
        <div className="site-layout-content">
          {
            !loadingClientInfo ? (<Descriptions title="Client Info">
                    <Descriptions.Item label="Client IP Address">
                      <Tag color='blue'>{clientInfo?.ipAddress}</Tag>
                    </Descriptions.Item>
                    <Descriptions.Item label="Browser Info">{clientInfo?.browserInfo}</Descriptions.Item>
                    <Descriptions.Item label="Operating System">{clientInfo?.operation}</Descriptions.Item>
                    <Descriptions.Item label="Referrer">{clientInfo?.referrer}</Descriptions.Item>
                    <Descriptions.Item label="Visit timezone">
                      {moment(clientInfo?.lastVisitTime, "x").tz(timezone).format()}
                    </Descriptions.Item>
                    <Descriptions.Item label="Visit Count">{clientInfo?.visitAmount}</Descriptions.Item>
                    <Descriptions.Item label="User Agent">{clientInfo?.userAgent}</Descriptions.Item>
                  </Descriptions>)
                  : <Skeleton/>
          }
          <Divider>Find Visitor by IP Address</Divider>
          <Form
            form={form}
            name="basic"
            onFinish={handleSearchClientByIp}
            autoComplete="off"
          >
            <Row gutter={24}>
              <Col span={19}>
                <Form.Item
                  name="ipAddress"
                  rules={[{ required: true, message: 'Please input IP Address' }]}
                >
                  <Input placeholder='Please input IP Address'/>
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item>
                  <Button type="primary" htmlType="submit" icon={<SearchOutlined />} loading={loadingFind}>
                    Find by IP
                  </Button>
                </Form.Item>
              </Col>
              <Col span={2}>
                <Form.Item>
                  <Button danger onClick={() => handleClearForm()}>
                    Clear
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Form>
          {
            findResult ? (<Descriptions title={<h2>Client IP: <Tag color='purple'>{findResult?.ipAddress}</Tag></h2>}>
                <Descriptions.Item label="Last visit timezone">
                  {moment(findResult?.lastVisitTime, "x").tz(timezone).format()}
                </Descriptions.Item>
                <Descriptions.Item label="Browser Info">{findResult?.browserInfo}</Descriptions.Item>
                <Descriptions.Item label="Visit Count">{findResult?.visitAmount}</Descriptions.Item>
                <Descriptions.Item label="Referrer">{findResult?.referrer}</Descriptions.Item>
                <Descriptions.Item label="Operating System">{findResult?.operation}</Descriptions.Item>
                <Descriptions.Item label="User Agent">{findResult?.userAgent}</Descriptions.Item>
              </Descriptions>) 
            : null
            
          }
          <Divider>Last 100 Visitors</Divider>
          <Table columns={getTableColumns(timezone)} dataSource={lastHundredVisitors} loading={loadingLast} />
          <Divider>Most 100 Visitors</Divider>
          <Table columns={getTableColumns(timezone)} dataSource={mostHundredVisitors} loading={loadingMost} />
        </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>©2022 Sky Mavis ThanhLe Interview</Footer>
    </Layout>
  );
}

export default App;
